# Terraform_Lab



## Install Terraform

### For Windows
download : https://www.terraform.io/downloads.html
```
- Unzip File Terraform (Ex. terraform_1.0.9_windows_amd64.zip)
- Open File Terraform
- Copy File terraform.exe
- Open Local Disk(C:)
- Create folder name : terraform-bins
- Open folder terraform-bins And Paste terraform.exe
- Add Environment Variable
- Choose Variable : Path --> Edit
- New --> C:\terraform-bins
- OK 
```
![windows](windows.png?raw=true)

```
terraform version
```
![windows2](cmd.png?raw=true)
### For Mac OS
download : https://www.terraform.io/downloads.html
```
unzip <PACKAGE-NAME>
unzip terraform_1.0.9_darwin_amd64.zip
```
Copy or Move terraform binary to /usr/local/bin
```
mv terraform /usr/local/bin
```

Test Command
```
terraform version
```
![mac](mac.png?raw=true)

